#+hugo_base_dir: ../
#+hugo_section: post
#+title: KML A GPS
#+lastmod: 2022-02-18
#+categories[]: rutas wikiloc
#+draft: false
#+tags: ["rutas" "wikiloc"]
#+author: Pedro Herrero García
#+date: <2022-02-18 vie>
* PASAR UN ARCHIVO DE RUTA .KML A .GPS
:LOGBOOK:
- State "FINALIZADO" from              [2022-02-17 jue 22:47]
:END:
Saludos. Este es un post real, en este sitio de desarrollo rural que
empieza. Me siento emocionado, y espero que resulte interesante, y
útil. Con ello cumpliría mis expectativas. Si ya algunos de mis
compañeros o compañeras técnicos de desarrollo rural se anima a
contribuir ello superaría el resultado esperado. Veremos :)\\
#+name: Ruta0
#+caption: "Uno de los mapas de Navarrevisca en Wikiloc"\\
\\
[[file:~/www/html/Desarrollo Rural/content/post/2022-02-16 Mapa ruta 0 wikiloc.jpg]]
\\
Tengo las rutas de senderismo para subir a [[https://es.wikiloc.com/wikiloc/spatialArtifacts.do?event=setCurrentSpatialArtifact&id=18342154][Wikiloc]] pero han
de estar en formato .gps (¿valdrá GPX?) y voy a intentar convertirlas
a dicho formato.\\
1. Intento utilidades online, pero me da error, y eso es porque es en
   realidad un dibujo de la ruta hecho con googlemymaps.
2. Encuentro esta web con una receta de comandos en bash (enlace) para
   ejecutar en el terminal linux. Mi portátil tiene instalado [[https://www.debian.org/index.es.html][Debian]]
   (enlace) que como todos los [[https://www.gnu.org/][GNU/linux]] tiene terminal de comandos. Esta
   es la web, llamada "[[https://gergolippai.com/2022/01/03/how-i-converted-google-mytrack-kmz-kml-to-strava-gpx-using-linux-and-gpsbabel/][A day in the rants]]". Esta es la lista de
   comandos, y cómo me ha salido a mí :)
   1. Crear carpeta: una copia de los tracks, y meterlos en una carpeta propia.
   2. Conseguir la información temporal, y eliminar los espacios y guiones, si los hay.
   #+begin_example
   #!/bin/sh
   for file in *.kml; do
   KMLDATETIME=`echo "$file" |awk '{print $1,substr($2,1,4)}'|sed 's/[- ]//g'`
   echo $KMLDATETIME
   FAKETIMEIN=f"$KMLDATETIME"00+6
   gpsbabel -i kml -f "$file" -x track,faketime=$FAKETIMEIN -o gpx -F "$file"_strava.gpx
   done
   #+end_example
   \\
   - La verdad, aunque he tomado el código de la web, no me ha funcionado.
3. Conclusión:
   1. Lo más fácil, volver a dibujar las rutas, cosa que se puede en wikiloc. Aunque llevará tiempo.
   2. Este experimento, también ha llevado tiempo. Que se ha ido en la
      experimentación. Para mí, no es tiempo desperdiciado. El
      problema es cuando vas sobrecargado de trabajo.
