## Bienvenida

Esta web es para promocionar mis clases de taijiquan, escribir acerca de las cosas que me suceden y forman parte de mi historia como practicante, estudiante y docente. El taijiquan es una estupenda puerta a la cultura milenaria china, junto con otras artes asociadas como el Qigong. Espero que sea un medio de comunicación fructífero.
