---
title: Quién soy yo
subtitle: Para que sepas algo del que escribe
comments: false
---

Soy Pedro Herrero García.

- Estudié biología en Oviedo, con un año en Irlanda como Erasmus.
- Vivo en Ávila, y trabajo como ADR en Navarrevisca
- Me gusta andar y leer
- Soy fan de GNU/linux
- También soy profe de Taijiquan

Puedes escribirme a pedro.herrero@navarrevisca.es

### Actualmente

Estoy centrado en mi trabajo de ADR, y quiero aplicar cosas del Taijiquan al desarrollo rural. Este sitio web es parte de ello.

Mi familia y mis amigos ocupan el resto de mi tiempo.
